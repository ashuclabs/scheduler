var previousDate = new Date();


exports.start_cron_job = start_cron_job;

function start_cron_job() {

    restartOn();
    var dateNow = new Date();


    dateNow.setMinutes(dateNow.getMinutes() + 20);
    var schedule = require('node-schedule');
    var rule = new schedule.RecurrenceRule();
    rule.hour = new schedule.Range(0, 59, 30);
    //rule.second = new schedule.Range(0, 59, 10);
    count = 0;
    var job = schedule.scheduleJob(rule, function() {
        console.log(rule);
        var date = new Date();
        console.log("hello " + (++count));
        console.log("time : " + (new Date()))
        restart(puts);

    });
}

function puts(error, stdout, stderr) {
    console.log(error);
    console.log(stderr);
    console.log(stdout);
}


function restartOn() {
    console.log("server restarted at " + previousDate.toString());
    var currentDate = new Date();
    console.log("server current time at " + currentDate.toString());
    previousDate.setMinutes(previousDate.getMinutes() + 120);
    console.log("server will restart on " + previousDate.toString());
}


var http = require("http");
var sys = require('sys');
var exec = require('child_process').exec;




function restart(callback) {

    exec(" whoami; pm2 restart all ; ", callback);

}

exports.restartAllProcess = restartAllProcess;

function restartAllProcess(req, res) {
    restart(puts);


    function puts(error, stdout, stderr) {
        if (error) {
            return res.send(error.toString());
        } else if (stdout) {
            return res.send(stdout.toString());
        } else {
            return res.send(stderr.toString());
        }

        console.log(error);
        console.log(stderr);
        console.log(stdout);
    }

}
